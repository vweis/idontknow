function createQuestions() {
	"use strict";
	
	
	//questions.push(new Question("Category", 0, "Question?", ["Answer1", "Answer2", "Answer3"], "correctAnswer"))
	// Create Math Question objects
	/*for (let i = 0; i <= 20; i++) {
		let question = new Question("Math", i, "What is 9 + 10?", ["21", "10", "19"], "21");
		if (i === 0) {question.active = true;}
		questions.push(question);
		let question2 = new Question("Science", (20 + i), "Is Bill Nye cool?", ["Probably", "Yes", "No"], "Probably");
		questions.push(question2);
	}*/
	
	questions.push(new Question("Factual", 0, "What are the three primary colors?", ["Purple, blue, green", "Orange, green, red", "Red, yellow, blue"], "Red, yellow, blue"));
	
	questions[0].active = true;
	
		questions.push(new Question("Factual", 1, "What is the distance around a circle called?", ["Diameter", "Circumference", "Length"], "Circumference"));

		questions.push(new Question("Factual", 2, "What is the product of 8 and 5?", ["40", "45", "35"], "40"));

		questions.push(new Question("Factual", 3, "What is the chemical symbol for Hydrogen?", ["Hy", "H", "G"], "H"));

		questions.push(new Question("Factual", 4, "What is the number called located on the bottom part of a fraction?", ["Denominator", "Division", "Numerator"], "Denominator"));

		questions.push(new Question("Factual", 5, "In which continent is the country of Egypt found?", ["Africa", "Asia", "India"], "Africa"));

		questions.push(new Question("Factual", 6, "Which particles are found in the nucleus of an atom?", ["Electrons", "Photons", "Neutrons"], "Neutrons"));

		questions.push(new Question("Factual", 7, "What is the exact value of pi?", ["3.14", "22/7", "None of these"], "None of these"));

		questions.push(new Question("Factual", 8, "Other than humans, what is the smartest mammal?", ["Dolphin", "Gorilla", "Elephant"], "Elephant"));

		questions.push(new Question("Factual", 9, "In addition to the 50 states, the USA has how many territories?", ["16", "20", "14"], "14"));

		questions.push(new Question("Factual", 10, "A natural number is doubled when added to its reciprocal. What is the number?", ["1/2", "2", "1"], "1"));

		questions.push(new Question("Factual", 11, "During the first six months of life, what color are a zebra's dark stripes?", ["Black", "Gray", "Brown"], "Brown"));

		questions.push(new Question("Factual", 12, "What is the force that holds back a sliding object?", ["Gravity", "Deceleration", "Friction"], "Friction"));

		questions.push(new Question("Factual", 13, "Force is often measured by what unit?", ["Joules", "Newtons", "Watts"], "Joules"));

		questions.push(new Question("Factual", 14, "Continue this pattern: 144, 121, 100, 81, 64...", ["42", "56", "49"], "49"));

		questions.push(new Question("Factual", 15, "At what temperature Fahrenheit does water boil?", ["212 degrees", "198 degrees", "204 degrees"], "212 degrees"));

		questions.push(new Question("Factual", 16, "Who developed the original idea of atoms?", ["Albert Einstein", "Niels Bohr", "Sir Isaac Newton"], "Niels Bohr"));

		questions.push(new Question("Factual", 17, "What does the C stand for in the equation E=mc^2?", ["Speed of Light", "Force of Gravity", "Change"], "Speed of Light"));

		questions.push(new Question("Factual", 18, "What is the 21st prime number?", ["73", "100", "96"], "73"));

		questions.push(new Question("Factual", 19, "Described as the basic relationship between scarcity and choice, the cost of choosing between mutually exclusive options is known as ___?", ["Opportunity Cost", "Market Shortage", "Decreasing Marginal Returns"], "Opportunity Cost"));

		questions.push(new Question("Factual", 20, "How do you calculate the density of a substance?", ["Mass*Volume", "Volume/Mass", "Mass/Volume"], "Mass/Volume"));

		questions.push(new Question("Factual", 21, "How many faces does an icosahedron have?", ["28", "20", "38"], "20"));

		questions.push(new Question("Factual", 22, "How many heads of executive departments make up the president's cabinet?", ["15", "25", "10"], "15"));

		questions.push(new Question("Factual", 23, "Who developed Calculus as a result of his work in Physics?", ["Stephen Hawking", "Albert Einstein", "Sir Isaac Newton"], "Sir Isaac Newton"));

		questions.push(new Question("Factual", 24, "Consider the following: A) All squares are rectangles. B) All rectangles have four sides. What can you conclude based on these statements?", ["All squares have four sides", "All rectangles are squares", "All squares have four corners", "Some squares have four sides"], "All squares have four sides"));

		questions.push(new Question("Complex", 25, "Which of the following characters may be numbers: 1, e, x, the square root of 8?", ["1 is the only real, rational, definite number", "1, e, and the square root of 8 are all numbers", "None of these are numbers unless specified in a larger context. They are only characters that may be used to represent quantities", "All of these may be numbers, depending on how they are used"], "1, e, and the square root of 8 are all numbers"));

		questions.push(new Question("Complex", 26, "What is time?", ["A measurement created by people in order to coordinate events across geographical distance", "The indefinite continued progress of existence and events in the past, present, and future regarded as a whole", "Distance measured from an intangible point from which duration is considered to run infinitely in both a positive and negative direction", "The act of planning, scheduling, or arranging and synchronizing events"], "None"));

		questions.push(new Question("Complex", 27, "Define the color green to the best of your ability.", ["The color of chlorophyl, uranium glass, and guacamole, among other things", "Light with a wavelength between approximately 560 and 520 nanometers", "When an object most strongly absorbs red light and thus reflects the opposite color of light back to a viewer, who perceives this reflected light", "A term used to describe a visual phenomenon"], "None"));

		questions.push(new Question("Complex", 28, "What is education?", ["The process of receiving or giving systematic instruction and information", "An enlightening experience that increases an individual or group’s understanding or ability", "The intended purpose of a school or university and the goal of a student", "The process of acquiring knowledge, a skill-set, and a degree for a specific profession"], "None"));

		questions.push(new Question("Complex", 29, "What accounts for the existence of such variety in biological species?", ["The process of less adapted variations of a species being killed off by their environment, inhibiting their reproduction", "The process of multiple random, synchronized mutations in genetic information over time that give certain individuals in a species an advantage", "The process of genetic information being shared and mixed across variations of species", "All of these are necessary"], "None"));

		questions.push(new Question("Complex", 30, "What is a number?", ["An arithmetical value expressed by a character, word, symbol, or figure", "An intangible quantity used in counting or operations represented by a recognizable, generally accepted tangible identifier", "A value or quantity that may be considered rational, irrational, real, or imaginary", "The act of quantifying or counting, or a state of being in which some amount of a subject is specified by a commonly accepted standard"], "None"));

		questions.push(new Question("Complex", 31, "Halfway through the semester, all of the students in your Math class stopped coming to the lectures. What can you conclude?", ["All students stop coming to lectures halfway through the semester", "All students stop coming to Math lectures halfway through the semester", "All of the students in your Math class withdrew from the course", "None of these can be concluded from this information"], "None"));

		questions.push(new Question("Abstract", 32, "What is freedom?", ["The ability to act, speak, or think as one desires without hindrance or restraint", "The absence of subjection to foreign domination or a despotic government", "Not being enslaved or imprisoned physically", "A mental or spiritual state in which one can live unhidden and unashamed, not suffering from internal conflict or subject to judgement"], "None"));

		questions.push(new Question("Abstract", 33, "Define the force of gravity to the best of your ability", ["The phenomenon of attraction between physical objects with mass or energy", "A quantity often represented by the symbol g, measured to be approximately 9.8 meters per second squared", "A force due to acceleration by which the earth is moving upwards, though more diminished than what people commonly think", "The force that keeps objects with mass or energy pulled down to the surface of the Earth"], "None"));

		questions.push(new Question("Abstract", 34, "Why do we perceive color?", ["Because it helps us recognize and differentiate between objects in the real world and find patterns in it which allow us to comprehend reality", "Because the idea and concept behind color instilled in us by society is cemented so fundamentally in our brains that we accept and define it as such", "Because it is both a practical and artistic aspect of the natural world created to be perceived, understood, and used", "There is no reason for our perception of color"], "None"));

		questions.push(new Question("Abstract", 35, "Consider the following two statements: A) Statement B is true, and B) Statement A is false. Which of the following must be the case?", ["Statement B is true and statement A is false", "Statement A is true and statement B is false", "Both statement A and statement B must be true", "Both statement A and statement B must be false"], "None"));

		questions.push(new Question("Abstract", 36, "What defines infinite?", ["Limitless or endless in either space, extent, or size", "Impossible to measure or calculate", "A characteristic of a theoretical quantity used for mathematical purposes", "A quality of having no starting point or ending point, but rather being perpetually continuous"], "None"));

		questions.push(new Question("Abstract", 37, "What accounts for the existence of natural laws?", ["Complete and random chance", "The design of an entity outside of and not subject to them", "The result of an infinite series of chain-reaction causes and events", "Natural laws are an illusion, and are actually not real at all"], "None"));

		questions.push(new Question("Abstract", 38, "What defines intelligence?", ["The ability to acquire and apply knowledge, to develop skills, and to learn and retain information long-term", "The faculty of reasoning and understanding objectively, including with respect to both academic and abstract matters", "The ability to think and act based on the remembrance of past experiences and observations", "To behave in such a way with discernment and awareness so as to ensure one’s own or others’ safety"], "None"));

		questions.push(new Question("Abstract", 39, "Is consciousness tangible?", ["Yes, everything can be tangibly perceived, even if we do not currently possess the ability to perceive acutely enough to perceive consciousness for ourselves", "Yes, consciousness is simply a complex mechanism, a quantum state of matter, which aids the perpetuation of human life like other mechanisms of the body", "No, consciousness cannot be sensed, cannot be held, and has no matter or mass, meaning it must be an intangible substance", "No, self-awareness necessitates consciousness, and both are abstract concepts which have no tangible instance but exist only in human thought"], "None"));

		questions.push(new Question("Abstract", 40, "If you were in a starving society and everyone agreed to eat you first, how should you respond?", ["It is better that one person die in order for more people to be preserved, so I should accept death in the hopes that others would survive.", "If everyone else agrees on this decision, then they must be correct and I should accept their decision.", "It is in my nature to preserve my own life, I should either fight them or run away.", "I should try to persuade them that human life was created sacred and that no one can justly kill someone else."], "None"));

		questions.push(new Question("Abstract", 41, "Where does the universe end?", ["The universe is circular, it bends back around on itself at some point in the same way that one travelling around the world in a singular direction will eventually end up back where they started", "The universe may have an end, but that end is constantly changing in location because the universe is continually expanding in space", "The universe ends and is surrounded by a static void, in which nothing exists", "The universe never ends, it stretches infinitely through space in all directions"], "None"));

		questions.push(new Question("Abstract", 42, "Where do thoughts come from?", ["Thoughts originate purely from within the human mind, they are a product of consciousness and the activity of the brain", "Thoughts come from outside our bodies, from different possible sources, and the human mind simply receives and deciphers them", "Thoughts are partially from outside experiences and observances and partially from the mind, which interprets and recombines them in new ways", "Thoughts do not come from anywhere or go anywhere, they simply exist perpetually and are perceived at times by the human mind"], "None"));

		questions.push(new Question("Abstract", 43, "Is there evidence that people share a common perception of reality?", ["We have the fact that humans can use language, and people can understand what one another are referring to clearly through communication", "We have vision and visual tests, in which people see the same objects or events, though the clarity with which people see may vary", "We have all of the senses, which work essentially the same across the world’s population, and which yield results that multiple people understand as similar", "There is no evidence for this, and in fact every single person’s perception of reality is different from every other person, though they may convince themselves otherwise"], "None"));

		questions.push(new Question("Abstract", 44, "What happens to you after you die?", ["Your being will be reincarnated into another life form on the earth repeatedly", "Your energy or life force goes out into the rest of the natural world leaving your body behind to decay, or your spirit lingers alongside the living", "You will lose all consciousness and cease to exist entirely", "Your core being and consciousness will live eternally in a place apart from the world, though you may have a new body"], "None" ));

		questions.push(new Question("Abstract", 45, "Will the physical world end, and how?", ["It will never end, it will have a chance to restore itself before that can happen", "It will be destroyed permanently, leaving behind or being replaced by a non physical realm", "Another physical world will be regenerated", "It will cease entirely, and nothing will remain"], "None"));

		questions.push(new Question("Abstract", 46, "If you do something illegal, when is it wrong?", ["If you are caught. Act in your best interest.", "If someone else is hurt by your actions", "If the moral code agreed upon by the society you are in has defined it as such.", "Law is a byproduct of morality. Your intrinsic moral conscience transcends it."], "None"));

		questions.push(new Question("Abstract", 47, "What defines a human soul?", ["A fictional spiritual entity that human minds have developed the concept of and which does not actually exist", "A spiritual entity derived from God’s spirit that resides within a person and is actually separate from their human self, sometimes causing a person to feel internal conflict", "The spiritual root and center of a person’s being that resides in their physical body, though it does not require a body, and makes a person who they are", "Another term for the mind or consciousness of a human or another living being, which makes them entitled to certain fundamental rights"], "None"));

		questions.push(new Question("Abstract", 48, "As humans, what is our purpose in life?", ["To survive, perpetuate our species, and evolve into more and more complex beings with greater and greater accomplishments even than what we have achieved thus far", "To do good and improve the world for everyone else by striving and working to become the best we can and making a positive rather than a negative impact that will change the world for the better, and likely put ourselves in a more positive place in the future by consequence", "To pursue happiness and contentment in life and live to the fullest that we possibly can, considering that we each only live once and for a limited period of time", "To love and serve the one who created us and gave us this life, and to draw others near along with us as we do so"], "None"));

		questions.push(new Question("Abstract", 49, "What best describes God?", ["A fictional being developed by people over time, allowing people to feel secure putting their faith in him and develop closely knit communities with others who share and reinforce faith in thei ame designated God", "Infinite and perfectly good, knowing everything and everyone, seeing everything, and having ultimate authority over the universe. He loves all people, and he designed them to love him as well", "Just like us, having a good side and a bad side, as he created all things that are both good and evil. He created the world and set it in motion, and he observes the work he created in the world for his own amusement" , "The universe and everything that exists in it. He split his being into millions of fragments and reflections of himself that took shape in every single part of everything, both tangible and intangible, that we see and know"], "None"));
	
		questions.push(new Question("Abstract", 50, "Consider this scenario: Three computers, A, B, and C exist such that one lies, one tells the truth, and one answers randomly. You must ask a total of three questions to any of the computers to figure out which is which, and each computer will answer 0 for no, 1 for yes. What must you ask?", ["Ask A: Does 1 mean yes if and only if you answer truthfully if and only if B answers randomly?", "Ask B: Does 0 mean yes if and only if you answer falsely if and only if C answers randomly?", "Ask C: Does 1 mean no if and only if A answers randomly if and only if you answer truthfully?", "Ask A: Does 0 mean no if and only if C answers randomly if and only if you answer falsely?"], "None"));



	
	console.log("Question objects generated.");
}
// JavaScript Document