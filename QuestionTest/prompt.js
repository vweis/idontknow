/*jshint esversion: 6 */



/*
-add timer
-refresh questions (optionally convert to JSONs)
-improve physical installation
-refine aesthetic to be more test-like
-change scale of questions
-??music??
-


*/

//https://www.awwwards.com/build-a-simple-javascript-app-the-mvc-way.html
		
		

var progress = 0;
var setNum = 0;
var questionLiftCheck = 0;
var fuckUp = false;
var accepted = 0;

/*function readSingleFile(evt) {
    //Retrieve the first (and only!) File from the FileList object
    var f = "/questions.txt"; 

    if (f) {
      var r = new FileReader();
      r.onload = function(e) { 
	      var contents = e.target.result;
        alert( "Got the file.n" 
              +"name: " + f.name + "n"
              +"type: " + f.type + "n"
              +"size: " + f.size + " bytesn"
              + "starts with: " + contents.substr(1, contents.indexOf("n"))
        );  
      }
      r.readAsText(f);
    } else { 
      alert("Failed to load file");
    }
  }*/

function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
	
	$(this.sound).addClass('sound');
    this.play = function(){
        this.sound.play();
    };
    this.stop = function(){
        this.sound.pause();
    };
}
		// Constructor function for Question objects
class Question {
	constructor(category, index, question, answers, accepted){
		this.active = false;
		
		this.category = category;
		this.index = index;

		this.question = question;

		this.answers = shuffle(answers); //randomize answer order
		this.yield = "I don\'t know";
		this.accepted = [];
		this.accepted.push(accepted);
		this.accepted.push(this.yield);

		this.answers.push(this.yield);
		this.answers = HTMLizeAnswers(this.answers, this.accepted);

		this.answerJoin = function() {
			let txt = "<br>" + this.answers.join("<br>");
			return txt;
			/*for x in this.answers {
				txt += answers[x] + "<br>"
			}*/
		};


	}
}

function shuffle(array) {
	"use strict";
	var currentIndex = array.length;
	var temporaryValue;
	var randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

	// Pick a remaining element...
	randomIndex = Math.floor(Math.random() * currentIndex);
	currentIndex -= 1;

	// And swap it with the current element.
	temporaryValue = array[currentIndex];
	array[currentIndex] = array[randomIndex];
	array[randomIndex] = temporaryValue;
	}

	return array;
}

function HTMLizeAnswers(answers, accepted) {
	"use strict";
	var txt = [];
	//var answer;
	for (var i in answers) {
		let answer = answers[i];
		
		if (accepted.indexOf(answer) >= 0) {
			txt.push("<button class='answer accepted' value='" + answers[i] + "'>" + answers[i] + "</button>");
		}
		else {
			txt.push("<button class='answer' value='" + answers[i] + "'>" + answers[i] + "</button>");
		}

		/*for (j in accepted) {
			if (i == j) {
				txt.push("<button class='answer, accepted'>" + answers[i] + "</button>");
			}
			else {
				txt.push("<button class='answer'>" + answers[i] + "</button>");
			}*/
		}
	
	return txt; 
}


var dingSound;
var badSound;
var questions = [];

var timerTickState = 0;
let flag1 = 9;
let flag2 = 18;
let flag3 = 27;
let flag4 = 32;
let flag5 = 40;


function displayQuestions() {
	"use strict";
	
	var elem = document.querySelector('#display');
	elem.innerHTML += "<p class='acceptedTag'>" + accepted + "</p>";
	elem.innerHTML += "<p class='acceptedTagShadow'></p>";
	document.getElementsByClassName('acceptedTag')[0].style.fontSize = 45 + "px";
	document.getElementsByClassName('acceptedTag')[0].style.top = '15px';
	document.getElementsByClassName('acceptedTag')[0].style.left = '10px';
	for (let question of questions) {
		
		var activity;
		// Add content after the current markup
		if (question.active === true) {activity = "active";} 
		else {activity = "inactive";}
		
		elem.innerHTML += "<div class='" + question.category + " question " + activity + "' id='" + question.index + "'><h2>" + question.question + "</h2>" + question.answerJoin() + "</div>";

	}
	console.log("Questions added to HTML.");
	//dingSound = sound('ding.wav');
	//badSound = sound('getDonged.wav');
	
}


function load() {
	'use strict';
	displayQuestions();
	questionStyle(2500);
		
		
	var buttons = document.getElementsByTagName("button");
	for (var i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener('click', buttonListen);
	}
	
	console.log("HTML body is loaded.");

}


var maxInstanceHeight = 0;
function questionStyle(speedOfAnim) {
	'use strict';
	let questions = document.getElementsByClassName('question');
	
	//retrieve highest height of the questions
	
	for (var b in document.getElementsByClassName('answer')) {
		if (b.innerHTML == "I don't know") {
			b.style.opacity = "0.0";
		}
	}
	
	
	if (questions.length >= 8 && progress < 1) {
		
		for (var j = 0; j < 8; j++) {
			let h = questions[j].offsetHeight;
			if (h > maxInstanceHeight){
				maxInstanceHeight = (h*9/10) - 15 - (progress/6);
			}
			questions[j].style.transitionDuration = speedOfAnim/1000 + 's';
		}
	}
	var hOffset = 0;
	
	//Moves all questions into place for the next phase.
	for (var i = 0; i < questions.length; i++) {
		var q = questions[i];
		let h = maxInstanceHeight;
		let topChange = hOffset/* - (maxInstanceHeight * (setNum - 1) + 10)*/ + "px";
		q.style.top = topChange;
		console.log(topChange);
		if (i === 0) {
			hOffset = hOffset + h;
		}
		if ($(q).hasClass('active')) {
			q.style.top = (10) + "px";
		}
		let center = ((window.innerWidth - q.clientWidth)/2) + 'px';
		let questionWidth = q.clientWidth;
		let marginOffset = 10;
		
		//Process for organizing button layout at different phases of the program.
		if (progress < flag1) {
			
			var videoOne = document.getElementById("animation");
			videoOne.play();
			
			 /*else if (i === 0) {
				q.style.left = (marginOffset) + 'px';
			} */

			if (q.id % 2 === 1) {
				q.style.left = ((window.innerWidth*1/4) - (questionWidth/2)+ marginOffset) + 'px';
				hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 2 === 0) {
				q.style.left = ((window.innerWidth*3/4) - (questionWidth/2)+ marginOffset) + 'px';
				//hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			}
			
		}
		else if (progress >= flag1 && progress < flag2) {
			
			if (q.id % 3 === 2) {
				q.style.left = (marginOffset) + 'px';
				hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 3 === 1) {
				q.style.left = center;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 3 === 0) {
				q.style.left = (window.innerWidth - questionWidth + marginOffset).toString() + 'px';
				//hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			}
			
		}
		else if (progress >= flag2 && progress < flag3) {
			
			var videoOne = document.getElementById("animation");
			var videoTwo = document.getElementById("animationmed");
			var divOne = document.getElementById("vid1");
			videoOne.pause();
			divOne.style.display = 'none';
			videoTwo.play();
			
			timerTickState = 1;
			if (q.id % 4 === 3) {
				q.style.left = ((window.innerWidth*1/8) - (questionWidth/2)+ marginOffset) + 'px';
				hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 4 === 2) {
				q.style.left = ((window.innerWidth*3/8) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 4 === 1) {
				q.style.left = ((window.innerWidth*5/8) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 4 === 0) {
				q.style.left = ((window.innerWidth*7/8) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			}
			
		} 
		else if (progress >= flag3 && progress < flag4) {
			
			if (q.id % 5 === 4) {
				q.style.left = ((window.innerWidth*1/10) - (questionWidth/2)+ marginOffset) + 'px';
				hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 5 === 3) {
				q.style.left = ((window.innerWidth*3/10) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 5 === 2) {
				q.style.left = ((window.innerWidth*5/10) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 5 === 1) {
				q.style.left = ((window.innerWidth*7/10) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 5 === 0) {
				q.style.left = ((window.innerWidth*9/10) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			}
			
		} 
		else if (progress >= flag4 && progress < flag5) {
			
			var videoTwo = document.getElementById("animationmed");
			var videoThree = document.getElementById("animationfast");
			var divTwo = document.getElementById("vid2");
			videoTwo.pause();
			divTwo.style.display = 'none';
			videoThree.play();
			
			timerTickState = 2;
			if (q.id % 8 === 7) {
				q.style.left = ((window.innerWidth*1/16) - (questionWidth/2)+ marginOffset) + 'px';
				hOffset = hOffset + h;
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 6) {
				q.style.left = ((window.innerWidth*3/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 5) {
				q.style.left = ((window.innerWidth*5/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 4) {
				q.style.left = ((window.innerWidth*7/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 3) {
				q.style.left = ((window.innerWidth*9/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 2) {
				q.style.left = ((window.innerWidth*11/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 1) {
				q.style.left = ((window.innerWidth*13/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			} else if (q.id % 8 === 0) {
				q.style.left = ((window.innerWidth*15/16) - (questionWidth/2) + marginOffset).toString() + 'px';
				if ($(q).hasClass('active')) {
					q.style.left = center;
				}
			}
			
		}
		//________________________________________________________________________
	}
	//__________________________________________________
	progress = progress + 1;
}


function buttonListen(e) {
	'use strict';
	
	if (timerActive === false) {
		timer = setInterval(countdown, 1000);
		document.getElementById("timer").style.opacity = "1.0";
		timerActive = true;
			document.getElementsByClassName("sound")[2].loop = true;
			//document.getElementsByClassName("sound")[2].play();
			
	}
	
	if (timerActive === true) {
		if (timerTickState === 1) {
			
			document.getElementsByClassName("sound")[2].pause();
			document.getElementsByClassName("sound")[2].loop = false;
			if (document.getElementsByClassName("sound")[3].paused === false){
				document.getElementsByClassName("sound")[3].loop = true;
				//document.getElementsByClassName("sound")[3].play(); 
			}
				
			
		} else if (timerTickState === 2) {
			document.getElementsByClassName("sound")[3].pause();
			document.getElementsByClassName("sound")[3].loop = false;
			if (document.getElementsByClassName("sound")[4].paused === false){
				document.getElementsByClassName("sound")[4].loop = true;
				//document.getElementsByClassName("sound")[4].play(); 
			}
		}
	}
	
	let center = (window.innerWidth - e.target.parentElement.clientWidth);
	let animSpeed = 2500 - (progress * 50);
	
	if ($(e.target).hasClass("accepted") === true && $(e.target.parentElement).hasClass("active") === true) {
		
		
		
		
		//Makes the correct button glow green.
		let buttonGlowSpeed = animSpeed/5;
		if (fuckUp === false) {
			//document.getElementsByClassName('sound')[0].play();
			let buttonGlowGreen = anime({
				targets: e.target,
				boxShadow: [
				{value: '0px 0px 5px rgba(40,212,25,0.75)'},
				{value: '0px 0px 10px rgba(40,212,25,0.75)'}
				],
				duration: buttonGlowSpeed,
				loop: false,
				easing: 'easeOutCubic'
			});
		}
		//____________________________________
		
		
		var nextQuestion = document.getElementsByClassName('inactive')[0];
		$(nextQuestion).addClass('active');
		$(nextQuestion).removeClass('inactive');
		if (nextQuestion == null){
			var videoThree = document.getElementById("animationfast");
			videoThree.pause();
			alert("Do you know?");
		}
		$(e.target.parentElement).addClass('done');
		
		$(e.target.parentElement).removeClass('question');
		
		//var completedQuestion = document.getElementsByClassName('done')[0];
		
		//nextQuestion = document.querySelector('.next');
		
		//let questionHeight = $(nextQuestion).height();
		
		
		
		let completedQuestionMove = anime({
			targets: '.done',
			translateY: '-70vh',
			duration: animSpeed,
			loop: false,
			direction: 'alternate',
			easing: [0.13, 0.79, 0.21, 0.93],
			opacity: {
				value: 0,
				delay: 100,
				duration: animSpeed - 100,
				easing: [0.13, 0.79, 0.21, 0.93]
			},
			complete: function() {
				$('.done').removeClass('active');
				$(e.target.parentElement).remove();
				console.log("Answered question spliced.");
			}
		
		});
		
		if (progress === flag1 || progress === flag2 || progress === flag3) {
			setNum = 0;
		}
		
		
		questionLiftCheck = questionLiftCheck + 1;
		/*if (progress < 12) {
			
			if (questionLiftCheck === 2) {
				setNum = setNum + 1;
				questionLiftCheck = 0;
				console.log("shifting shit (p1)");
			}
			
		} else if (progress >= 12 && progress < 24) {
			
			if (questionLiftCheck === 3) {
				setNum = setNum + 1;
				questionLiftCheck = 0;
				console.log("shifting shit (p2)");
			}
			
		} else if (progress >= 24 && progress < 44) {
			
			if (questionLiftCheck === 4) {
				setNum = setNum + 1;
				questionLiftCheck = 0;
				console.log('shifting shit (p3)');
			}
			
		}*/
		if (fuckUp === false){
			let storedAccepted = accepted;
			accepted = accepted + 1;
			
			let succTag = document.getElementsByClassName('acceptedTag')[0];
			succTag.style.fontSize = (45 - progress/2) + "px";
			succTag.innerHTML = accepted;
			
		}
		
		//progress = progress + 1;
		console.log('number of questions passed:' + progress);
		questionStyle(animSpeed);
		fuckUp = false;
		darkenInstall();
	} else {
		
		document.getElementsByClassName('sound')[1].play();
		
		let animSpeed = 2500 - (progress * 40);
		
		//Makes the correct button glow green.
		let buttonGlowSpeed = animSpeed/5;
		
		let buttonGlowRed = anime({
			targets: e.target,
			boxShadow: [
			{value: '0px 0px 3px 0 rgba(212,40,25,0.75)'},
			{value: '1px 2px 15px 9px rgba(212,40,25,0.75)'}
			],
			duration: buttonGlowSpeed,
			loop: false,
			easing: 'linear'
		});
		
		timeLeft = timeLeft - 5;
		if (fuckUp == false) {
			fuckUp = true;
		} else {
			var nextQuestion = document.getElementsByClassName('inactive')[0];
			$(nextQuestion).addClass('active');
			$(nextQuestion).removeClass('inactive');

			$(e.target.parentElement).addClass('done');

			$(e.target.parentElement).removeClass('question');

			//var completedQuestion = document.getElementsByClassName('done')[0];

			//nextQuestion = document.querySelector('.next');

			//let questionHeight = $(nextQuestion).height();



			let completedQuestionMove = anime({
				targets: '.done',
				translateY: '-70vh',
				duration: animSpeed,
				loop: false,
				direction: 'alternate',
				easing: [0.13, 0.79, 0.21, 0.93],
				opacity: {
					value: 0,
					delay: 100,
					duration: animSpeed - 100,
					easing: [0.13, 0.79, 0.21, 0.93]
				},
				complete: function() {
					$('.done').removeClass('active');
					$(e.target.parentElement).remove();
					console.log("Answered question spliced.");
				}

			});

			if (progress === 12 || progress === 24 || progress === 44) {
				setNum = 0;
			}
			progress = progress + 1;

			questionLiftCheck = questionLiftCheck + 1;

			//progress = progress + 1;
			console.log('number of questions passed:' + progress);
			questionStyle(animSpeed);
			fuckUp = false;
			darkenInstall();
		}
		console.log("You're wrong, loser");
	}

}

// Set the date we're counting down to
//var countDownDate = new Date("Jan 5, 2019 15:37:25").getTime();
var countDownDate = new Date().getTime();

var timerActive = false;
var timeLeft = 180;
// Update the count down every 1 second

var timer;

var promptActive = false;
function countdown(){
	if (timerActive === true) {
		if (timeLeft > 0) {
			timeLeft = timeLeft - 1;
			console.log('tick..');
			let minutes = Math.floor(timeLeft/60);
			let seconds = Math.floor(timeLeft % 60);
			// Display the result in the element with id="timer"
			document.getElementById("timer").innerHTML = minutes + ":" + seconds;
		} 
		if (timeLeft < 120) {
			promptActive = true;
		}
// If the count down is finished, write some text 
		else if (timeLeft < 0) {
			clearInterval(timer);
			timerActive = false;
			document.getElementById("timer").innerHTML = "EXPIRED";
			document.getElementById("timer").style.opacity = "0.0";
		}
	}
}
//var active = document.querySelector(".active")
/*var activeAnim = anime({
	targets: '#QuestionBox .active',
	translateX: 100
});*/
function hue2rgb(p, q, t){
	if(t < 0) {t += 1;}
	if(t > 1) {t -= 1;}
	if(t < 1/6) {return p + (q - p) * 6 * t;}
	if(t < 1/2) {return q;}
	if(t < 2/3) {return p + (q - p) * (2/3 - t) * 6;}
	return p;
}

function hslToRgb(h, s, l){
	var r, g, b;



	if(s == 0){
		r = g = b = l; // achromatic
	}else{


		var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
		var p = 2 * l - q;
		r = hue2rgb(p, q, h + 1/3);
		g = hue2rgb(p, q, h);
		b = hue2rgb(p, q, h - 1/3);
	}

	return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

function darkenInstall() {
	let animStart = 7;
	let animStop = 40;
	let percentMap = (progress - animStart)/51;
	
	
	

	
	
	if (progress > animStart && progress < animStop){
		//let backgroundColor = document.getElementById("questions").style.backgroundColor;
		//let questionColor = document.getElementsByClassName("question").style.backgroundColor;
		//let buttonColor = document.getElementsByClassName("answer").style.backgroundColor;

		
		
		var cBGColorNum = hslToRgb(((43/255) - ((43/255) * (percentMap))),(65 - (65 * (percentMap))),(69 - (69 * (percentMap))));
		//let cQColor = 
		//let cBColor =
		
		var cBGColorF = "rgba(" + cBGColorNum[0] + "" + cBGColorNum[1] + "" + cBGColorNum[2] + ", 1.0)"
		
		
		
		$('#questions').css('background-color', cBGColorF.toString());
		/*"hsl(" + ((43/255) - ((43/255) * (percentMap))) + "," + (65 - (65 * (percentMap))) + "%," + (69 - (69 * (percentMap))) + "%,1.0) !important"*/
		//$('.question').css('background-color', '#FF6600');
		//$('.answer').css('background-color', '#FF6600');
		
		//backgroundColor = "hsl(" + (43 - (43 * (percentMap))) + "," + (65 - (65 * (percentMap))) + "," + (69 - (69 * (percentMap))) + ",1.0)";
																											  
		//"hsl(" + (INIT - (TARGET * (percentMap))) + "," + (INIT - (TARGET * (percentMap))) + "%," + (INIT - (TARGET * (percentMap))) + "%,1.0)"
		
	}
	
}